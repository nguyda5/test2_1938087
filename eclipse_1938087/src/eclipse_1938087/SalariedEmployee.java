package eclipse_1938087;

public class SalariedEmployee implements Employee{
	private int yearlyPay;
	public SalariedEmployee(int yearlyPay) {
		this.yearlyPay = yearlyPay;
	}
	public int getYearlyPay() {
		return this.yearlyPay;
	}
}
