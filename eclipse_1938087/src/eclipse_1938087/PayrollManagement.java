package eclipse_1938087;

/**
 * PayrollManagement manages all the employee's salary
 * @author David Nguyen
 *
 */
public class PayrollManagement {
	/**
	 * getTotalExpenses calculates the total amount of expenses.
	 * @param allEmployees An array of employees
	 * @return the total expenses
	 */
	public static int getTotalExpenses(Employee[] allEmployees) {
		int totalExpenses = 0;
		for(int i = 0; i < allEmployees.length; i++) {
			//adds each employee yearlyPay to totalExpenses
			totalExpenses += allEmployees[i].getYearlyPay();
		}
		return totalExpenses;
	}
	
	public static void main(String[]args) {
		Employee[] allEmployees = new Employee[5];
		
		//hard coded employee salaries
		allEmployees[0] = new SalariedEmployee(500);
		allEmployees[1] = new HourlyEmployee(25,14);
		allEmployees[2] = new UnionizedHourlyEmployee(26, 17,6252);
		allEmployees[3] = new HourlyEmployee(12,17);
		allEmployees[4] = new UnionizedHourlyEmployee(0, 17,2310);
		
		System.out.println("Total expenses: " + getTotalExpenses(allEmployees));
 	}
}
