package eclipse_1938087;

/**
 * HourlyEmployee is a type of Employee. It has an hourly pay and the number of hours worked per week.
 * @author David Nguyen
 *
 */
public class HourlyEmployee implements Employee{
	private int hoursWorkedPerWeek;
	private int hourlyPay;
	
	/**
	 * HourlyEmployee constructor
	 * @param hoursWorkedPerWeek Number of hours worked per week
	 * @param hourlyPay Hourly wage
	 */
	public HourlyEmployee(int hoursWorkedPerWeek, int hourlyPay) {
		this.hoursWorkedPerWeek = hoursWorkedPerWeek;
		this.hourlyPay = hourlyPay;
	}
	
	/**
	 * getYearlyPay returns the annual salary
	 * @return annual salary;
	 */
	public int getYearlyPay() {
		return hoursWorkedPerWeek*hourlyPay*52;
	}
}
