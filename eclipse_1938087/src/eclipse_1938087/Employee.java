package eclipse_1938087;

/**
 * All employees have a yearlyPay.
 * @author David Nguyen
 *
 */
interface Employee {
	/**
	 * @return annual salary
	 */
	public int getYearlyPay();
}
