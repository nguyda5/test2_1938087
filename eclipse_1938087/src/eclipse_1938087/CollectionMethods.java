package eclipse_1938087;

import java.util.*;

/**
 * @author David
 */
public class CollectionMethods {
	/**
	 * Gets the first three planets.
	 * @param planets All planets given
	 * @return the first three planets
	 */
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		Collection<Planet> firstThree = new ArrayList<Planet>();
		
		//goes through all the planets and add only the first three
		for(Planet currentPlanet : planets) {
			if(currentPlanet.getOrder() == 1 || currentPlanet.getOrder() == 2 || currentPlanet.getOrder() == 3 ) {
				firstThree.add(currentPlanet);
			}
		}
		
		return firstThree;
	}
}
