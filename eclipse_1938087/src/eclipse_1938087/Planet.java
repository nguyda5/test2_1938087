package eclipse_1938087;
/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	//Question 3
	/**
	 * @return If the both planets are the same.
	 */
	public boolean equals(Object o) {
		//verifies if the object is a planet before comparing
		if(!(o instanceof Planet)) {
			return false;
		}
		//verifies if planet name and order are identical
		return(this.name.equals(((Planet)o).name) && this.order == ((Planet)o).order);
	}
	/**
	 * @return Int representation of the Planet object based off the name and order.
	 */
	public int hashCode() {
		int id = 0;
		//gets an int representation of the planet name by adding all chars together.
		//case is not sensitive (Saturn will be equal to saturn)
		for(int i = 0; i < this.name.length(); i++) {
			id += (int)name.toLowerCase().charAt(i);	
		}
		id += order;
		return id;
	}
	/**
	 * @return If a planet is higher, lower or equal to another planet based off name and planetary system name.
	 */
	public int compareTo(Planet currentPlanet) {
		//compares the planet name first
		int nameCheck = this.name.toLowerCase().compareTo(currentPlanet.name.toLowerCase());
		//if they're the same, compare the planetary system name
		if(nameCheck == 0) {
			return this.planetarySystemName.compareTo(currentPlanet.planetarySystemName);
		}
		return nameCheck;
	}

	
}
