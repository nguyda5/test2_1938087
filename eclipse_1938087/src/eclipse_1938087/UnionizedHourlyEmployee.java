package eclipse_1938087;

/**
 * UnionizedHourlyEmployee is a class that extends HourlyEmployee.
 * @author David Nguyen
 *
 */
public class UnionizedHourlyEmployee extends HourlyEmployee{
	private int pensionContribution;
	/**
	 * UnionizedHourEmployee constructor
	 * @param hoursWorkedPerWeek Hours worked per week
	 * @param hourlyPay Hourly wage
	 * @param pensionContribution Extra money
	 */
	public UnionizedHourlyEmployee(int hoursWorkedPerWeek, int hourlyPay, int pensionContribution) {
		super(hoursWorkedPerWeek, hourlyPay); //calls to the HourlyEmployee constructor
		this.pensionContribution = pensionContribution;
	}
	/**
	 * getYearlyPay returns annual salary with pensionContribution
	 * @return annual salary
	 */
	public int getYearlyPay() {
		return super.getYearlyPay() + pensionContribution;
	}
	
}
